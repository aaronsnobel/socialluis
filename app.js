
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/socketio', express.static(__dirname + '/node_modules/socket.io-client/dist/'));
var socialLoginClass = require("social-login");
var socialLogin = new socialLoginClass({
    app: app,    // ExpressJS instance
    url: 'https://expressjs.snobel.ca',  // Your root url
    onAuth: function (req, type, uniqueProperty, accessToken, refreshToken, profile, done) {

        // This is the centralized method that is called when the user is logged in using any of the supported social site.
        // Setup once and you're done.

        done(null, profile);   // Return the user and continue
        
    }
});

// Setup the various services:
socialLogin.use({
    facebook: {
        settings: {
            clientID: "2037384379921007",
            clientSecret: "142a0ac82b0e54dfc7c20af808d2947a",
            authParameters: {
                scope: 'public_profile, email'
            }
        },
        url: {
            auth: "/auth/facebook",           // The URL to use to login (<a href="/auth/facebook">Login with facebook</a>).
            callback: "/auth/facebook/callback",  // The Oauth callback url as specified in your facebook app's settings
            success: '/success',                        // Where to redirect the user once he's logged in
            fail: '/auth/facebook/fail'       // Where to redirect the user if the login failed or was canceled.
        }
    },
    google:	{
        settings:	{
            clientID:'494288397962-b4neot39ag0vdtjjp510uuk6usk6t8fr.apps.googleusercontent.com',
            clientSecret: 'STncbhA54TaKaKyDsIXuBOuw',
            authParameters:{scope:'https://www.googleapis.com/auth/userinfo.email'}
        }, // Google doesn't take any API key or API secret
        url:	{
            auth:		"/auth/google",
            callback: 	"/auth/google/callback",
            success:	'/success',
            fail:		'/auth/google/fail'
        }
    }
});


app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Init

module.exports = app;
