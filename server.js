#!/usr/bin/env node

/**
 * Module dependencies.
 */
 var fs = require('fs');
var app = require('./app');
var debug = require('debug')('socialluis2:server');
var http = require('http');
var server = http.createServer(app);
var io = require('socket.io')(server);
var request = require('requestretry');

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */




/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
io.sockets.on('connection', function (socket) {
    socket.on('GetIntent', function (data) {
        GetIntent(data, socket);
    });
    socket.on("GetKey", function (data) {
        socket.emit("retKey", "029dbdf857d24398b096b97711b13b27");
    });
});
/**
 * Normalize a port into a number, string, or false.
 */
var retryStrategy = function (err, response, body) {
    let shouldRetry = err || (response.statusCode === 429);
    if (shouldRetry) console.log("retrying add intent...");
    return shouldRetry;
}

function GetIntent(query,socket) {
    console.log("in intent");
    var params = {
        // These are optional request parameters. They are set to their default values.
        "timezoneOffset": "0",
        "verbose": "false",
        "spellCheck": "false",
        "staging": "false",
    };
    // time delay between requests
    const delayMS = 1000;

    // retry recount
    const maxRetry = 5;
    var data =  request({
        url: "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/b6e18915-8bd9-48b3-8775-1fdacc01fbe2?q=" + query,
        json: true,
        headers: {
            // Request headers
            "Ocp-Apim-Subscription-Key": "9cd7bfc850f444e68e773e1f4e9fcf97"
        },
        method: "GET",
        // The request body may be empty for a GET request
        body: "{body}",
        maxAttempts: maxRetry,
        retryDelay: delayMS,
        retryStrategy: retryStrategy
    }, function (err, response, body) {
        // this callback will only be called when the request succeeded or after maxAttempts or on error
        socket.emit("messages", body);
        console.log("emitted");
    });
        
    
}
function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
