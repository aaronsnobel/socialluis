﻿var client;

function getKey() {
    socket.emit("GetKey");
}

function setText(text) {
    console.log(text);
    socket.emit('GetIntent', text);
    $("#chat_div").chatbox("option", "boxManager").addMsg("speecrecog", text);
}

function start() {
    document.getElementById("startBtn").disabled = true;
    document.getElementById("stopBtn").disabled = false;
    client.startMicAndContinuousRecognition();
}

function stop() {
    document.getElementById("startBtn").disabled = false;
    document.getElementById("stopBtn").disabled = true;
    client.endMicAndContinuousRecognition();
}

function createAndSetupClient(theKey) {
    document.getElementById("startBtn").disabled = false;

    if (client) {
        stop();
    }

    client = new BingSpeech.RecognitionClient(theKey);

    client.onFinalResponseReceived = function (response) {
        setText(response);
    }

    client.onError = function (code, requestId) {
        console.log("<Error with request n°" + requestId + ">");
    }

   
}