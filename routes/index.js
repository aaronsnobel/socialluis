var express = require('express');
var router = express.Router();
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/success', function (req, res, next) {
    res.render('success', { title: 'Success' });
});

router.get('/secret', function (req, res, next) {
  res.send("read the console");
  
});
module.exports = router;
